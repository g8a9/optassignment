#include <list>
#include <iostream>
#include <cstring>
#include "utils.h"
#include "heuristic.h"

using namespace std;

/*Program should be launched at least with paths of input and output files
	as parameters. Optionally you can use the "test mode" */

int main(int argc,char *argv[]){
	bool _test = false;
	string _inPath;
	string _outPath;
	string _solPath;
	const string _optPath = "optimal_solutions.csv";

	// Read input parameters
	for(int i=1; i< argc;i++) {
		if(strcmp(argv[i], "-i") == 0)
			_inPath = argv[++i];
		else if(strcmp(argv[i], "-o") == 0)
			_outPath = argv[++i];
		else if(strcmp(argv[i], "-s") == 0)
			_solPath = argv[++i];
		else if(strcmp(argv[i], "-test") == 0)
			_test = true;
	}

	// If you are not in test mode and one of input or output path is missing
	if(!_test && (_inPath.empty() || _outPath.empty())) {
		cout << "------------------------------ " << endl;
		cout << "CMD" << endl;
		cout << "------------------------------ " << endl;
		cout << "-i path of the instance file" << endl;
		cout << "-o path of the output file" << endl;
		cout << "-s path of the output solution file or of the solution to test(optional)" << endl;
		cout << "-test enable the feasibility test (optional)" << endl;
        return 1;
	}

	if(!_test) {
		// Read the instance file
		Heuristic _heuristic(_inPath);
		// Solve the problem
		vector<double> stat;
		//_heuristic.solveFast(stat);
		//_heuristic.greedySolve(stat);
		_heuristic.greedyRecursive();

		_heuristic.getStatSolution(stat);


		// Write KPI of solution
    string instanceName = splitpath(_inPath);
	_heuristic.checkOptimalityGap(_optPath, instanceName, stat); //Stampo a video la percenutale
    _heuristic.writeKPI(_outPath, instanceName, stat);
		// Write solution
		if(!_solPath.empty())
		_heuristic.writeSolution(_solPath);
	}
	else {

		/*Test mode is used for testing solution already computed in a previous
			run in order to get their feasibility/unfeasibility.*/

    // Read the instance file
		Heuristic _heuristic(_inPath);
		// Read the solution file
		eFeasibleState _feasibility = _heuristic.isFeasible(_solPath);
		switch(_feasibility) {
			case FEASIBLE:
				cout << "Solution is feasible" << endl;
				break;
			case NOT_FEASIBLE_DEMAND:
				cout << "Solution is not feasible: demand not satisfied" << endl;
				break;
			case NOT_FEASIBLE_USERS:
				cout << "Solution is not feasible: exceeded number of available users" << endl;
				break;
		}
	}

	return 0;
}
