#include <iostream>
#include <random>
#include <algorithm>
#include <ctime>
#include <cmath>
#include <string>
#include <limits>
#include "heuristic.h"

using namespace std;

Heuristic::Heuristic(string path){
    this->hasSolution = false;
    string line;
    string word;

    /*  ifstream é una classe di input stream che lavora sui file
        path.c_str() ritorna la stessa stringa terminata da '\0'
    */
    ifstream iffN(path.c_str());

    if (!iffN.is_open()) {
        cout << "Impossible to open" << path << endl;
        cin.get();
        exit(1);
    }

    /*
        Legge dal file la prima riga, sulla stessa apre uno stream e lo scorre con un iteratore salvando il numero di celle dell'istanza, di tempi e di tipi di persone
        ..std::replace cambia in tutte le occorenze nel range specificato, ';' con ' '
    */
    getline(iffN, line);
    std::replace(line.begin(), line.end(), ';', ' ');
    istringstream iss(line);
    iss >> word;
    this->nCells = atoi(word.c_str());
    iss >> word;
    this->nTimeSteps = atoi(word.c_str());
    iss >> word;
    this->nCustomerTypes = atoi(word.c_str());

    /*
        Effettua l'allocazione della memoria:
        L'oggetto 'problem' contiene tutte le informazioni sul problema:
        ..costs: matrice a 4 dimensioni. L'elemento (i,j,m) identifica il costo di uno spostamento nella cella 'j', a partire dalla cell'i', di un utente di tipo 'm', infine l'elemento stesso rappresenta un puntatore al vettore dei tempi. La stessa allocazione é fatta su interi per l' oggetto 'solution'
        ..n: vettore che contiene i tipi di utenti. Credo che questo numero sia l'"n" che nelle specifiche indica il numero di task che puo' compiere un tipo m nella stessa cella
        ..activities: vettore che contiene per ogni cella il numero di attivitá da svolgere su essa
        ..usersCell: matrice a 3 dimensioni. L'elemento (i,m) indica il numero di utenti di tipo 'm' nella cella 'i', infine l'elemento rappresenta un puntatore al vettore dei tempi.
    */
    solution = new int***[nCells];
    problem.costs = new double***[nCells];
    for (int i = 0; i < this->nCells; i++) {
        problem.costs[i] = new double**[nCells];
        solution[i] = new int**[nCells];
        for (int j = 0; j < this->nCells; j++) {
            problem.costs[i][j] = new double*[nCustomerTypes];
            solution[i][j] = new int*[nCustomerTypes];
            for (int m = 0; m < this->nCustomerTypes; m++) {
                problem.costs[i][j][m] = new double[nTimeSteps];
                solution[i][j][m] = new int[nTimeSteps];
            }
        }
    }
    problem.n = new int[nCustomerTypes];
    problem.activities = new int[nCells];
    problem.usersCell = new int**[nCells];
    for (int i = 0; i < this->nCells; i++) {
        problem.usersCell[i] = new int*[nCustomerTypes];
        for (int m = 0; m < this->nCustomerTypes; m++) {
            problem.usersCell[i][m] = new int[nTimeSteps];
        }
    }

    /*Memorizza i tipi di utente*/
    getline(iffN, line);
    getline(iffN, line);
    std::replace(line.begin(), line.end(), ';', ' ');
    istringstream issN(line);
    for (int m = 0; m < nCustomerTypes; m++) {
        issN >> word;
        problem.n[m] = atoi(word.c_str());
    }

    /*  Memorizzazione della matrice dei costi con quattro cicli annidati. Vengono letti in ordine tutti i costi per ogni istante di tempo di un tipo di utente, poi si passa ad un altro tipo utente */
    getline(iffN, line);
    for (int m = 0; m < nCustomerTypes; m++) {
        for (int t = 0; t < nTimeSteps; t++) {
            getline(iffN, line);// linea con m e t
            for (int i = 0; i < nCells; i++) {
                getline(iffN, line);// linea della matrice c_{ij} per t ed m fissati
                istringstream issC(line);
                for (int j = 0; j < nCells; j++) {
                    issC >> word;
                    problem.costs[i][j][m][t] = atoi(word.c_str());
                }
            }
        }
    }

    /* Lettura del numero di attivitá da svolgere per ogni cella*/
    getline(iffN, line);
    getline(iffN, line);
    std::replace(line.begin(), line.end(), ';', ' ');
    istringstream issA(line);
    for (int i = 0; i < nCells; i++) {
        issA >> word;
        problem.activities[i] = atoi(word.c_str());
    }

    /* Lettura del numero di utenti per ciascuna cella ad ogni istante temporale, come prima si processa prima un tipo di utente e poi si passa ad un altro */
    getline(iffN, line);
    for (int m = 0; m < nCustomerTypes; m++) {
        for (int t = 0; t < nTimeSteps; t++) {
            getline(iffN, line);
            getline(iffN, line);
            std::replace(line.begin(), line.end(), ';', ' ');
            istringstream issU(line);
            for (int i = 0; i < nCells; i++) {
                issU >> word;
                problem.usersCell[i][m][t] = atoi(word.c_str());
            }
        }
    }

}

void Heuristic::evaluate(int i, int j, int m, int t, int &demand, double &objFunc, bool &satisfied) {
    if (demand > problem.n[m]*problem.usersCell[j][m][t]) {
        solution[j][i][m][t] = problem.usersCell[j][m][t];
    } else {
        solution[j][i][m][t] += ceil((float) demand / problem.n[m]);
    }

    if(solution[j][i][m][t] != 0) {
        problem.usersCell[j][m][t] -= solution[j][i][m][t];
        demand -= solution[j][i][m][t] * problem.n[m];
        //cout << "Expleted: " << solution[j][i][m][t] * problem.n[m] << endl;
        objFunc += solution[j][i][m][t] * problem.costs[j][i][m][t];
        if(demand <= 0)
            satisfied = true;
    }

}

void Heuristic::initSol(int ****& solution) {
    for (int i = 0; i < nCells; i++)
        for (int j = 0; j < nCells; j++)
            for (int m = 0; m < nCustomerTypes; m++)
                for (int t = 0; t < nTimeSteps; t++)
                    solution[i][j][m][t] = 0;
}

void Heuristic::greedySolve(vector<double>& stat) {
	int *completed = new int[nCells];
    double objFunc = 0;
    bool satisfied;
    int i, j, m, t;
    int to_do = 0;
    int distance = 10;
    int window = nCells; //Finestra di valutazione, DA MODIFICARE

    clock_t tStart = clock();
    this->initSol(solution);

    for(i = 0; i < nCells; i++) {
        if(problem.activities[i] != 0) {
        	completed[i] = 0;
        	to_do++;
        } else
            completed[i] = 1;
    }

    //Le condizioni del for sono per saltare qualche iterazione (completed) e per finire prima (to_do)
    for(i = 0; i < nCells && to_do != 0; i++) {
        if(!completed[i]) {

            //Parto dalla prima cella
            int demand = problem.activities[i];
            //cout << "Initial demand = " << demand << endl;
            satisfied = false;

            //Provo prima ad utilizzare gli utenti 'da pochi task' -- SI PUO' CAMBIARE
            while(!satisfied) {
                float min_cost = numeric_limits<float>::max();
                int min_j, min_m, min_t;

                for(m = nCustomerTypes - 1; m >= 0 && !satisfied; m--) {

                    //Celle adiacenti a dx
                    for(j = i+1; (j-i) < window && j < nCells && !satisfied; j++) {
                        for(t = 0; t < nTimeSteps && !satisfied; t++) {
                            if(problem.usersCell[j][m][t] != 0) { // Se ci sono persone nella cella
                                if(((float) problem.costs[j][i][m][t]/problem.n[m]) < min_cost) {
                                    min_j = j;
                                    min_m = m;
                                    min_t = t;
                                    min_cost = (float) problem.costs[j][i][m][t]/problem.n[m];
                                }
                            }
                        }
                    }

                    //Celle adiacenti a sx
                    for(j = i-1; (i-j) < window && j >= 0 && !satisfied; j--) {
                        for(t = 0; t < nTimeSteps && !satisfied; t++) {
                            if(problem.usersCell[j][m][t] != 0) { // Se ci sono persone nella cella
                                if(((float) problem.costs[j][i][m][t]/problem.n[m]) < min_cost) {
                                    min_j = j;
                                    min_m = m;
                                    min_t = t;
                                    min_cost = (float) problem.costs[j][i][m][t]/problem.n[m];
                                }
                            }
                        }
                    }
                }

                //Esco con il min_cost, devo valutarne la distanza dal minimo globale ovvero "come potrei utilizzare al meglio quelle persone"
                float min_glob = numeric_limits<float>::max();
                for(j = 0; j < nCells; j++)
                    if(j != min_j && j != i && problem.activities[j] != 0)  // Se ci sono task da compiere nella cella verso cui le utilizzerei al meglio
                        if(((float) problem.costs[min_j][j][min_m][min_t]/problem.n[m]) < min_glob)
                            min_glob = (float) problem.costs[min_j][j][min_m][min_t]/problem.n[m];

                //Distance DA MODIFICARE
                if(abs(min_glob - min_cost) < distance) {
                    evaluate(i, min_j, min_m, min_t, demand, objFunc, satisfied);
                } //else --> Vai al prossimo min_cost locale!

            }
            to_do--;
            completed[i] = 1;
        }
    }

    if(to_do == 0) { //Ho completato tutte le celle = feasible
        hasSolution = true;
        cout << "Feasible!" << endl;
        greedyImprove(solution, objFunc);

        // Inserisce nel vettore delle statistiche il valore della objFunc finale e il tempo impiegato
        stat.push_back(objFunc);
        double lastedTime = (double) (clock() - tStart) / CLOCKS_PER_SEC;
        stat.push_back(lastedTime);
        cout << "Tempo impiegato: " << ((double)(clock()-tStart) / CLOCKS_PER_SEC) << " s" << endl;
        cout << "Result = " << objFunc << endl;
    } else {
        cout << "Not feasible yet" << endl;
    }
}

void Heuristic::greedyImprove(int ****& solution, double& objFunc) {
	int i, j, m, t, q, r;
    int jl, ml, tl;
    int jr, mr, tr;
    int worst_l, worst_r;
    int task_todo;
    int people_to_move;

    //Trovo i peggiori
    i = 0;
    r = nCells-1;
    q = 0;
	while(q <= nCells/2) {
        people_to_move = 0;
        worst_l = 0;
		if(i != r) {
            for(j = 0; j < nCells; j++) //Si puo' cambiare e partire da j = i+1
                if(j != i && j != r)
                    for(m = 0; m < nCustomerTypes; m++)
                        for(t = 0; t < nTimeSteps; t++)
                            if(solution[i][j][m][t] != 0) {
                                int obj_gain = solution[i][j][m][t]*problem.costs[i][j][m][t];
                                if(obj_gain > worst_l) {
                                    worst_l = obj_gain;
                                    jl = j;
                                    ml = m;
                                    tl = t;
                                    task_todo = solution[i][j][m][t]*problem.n[m];
                                }
                            }
		}
        //Peggior apporto alla objFunc e' solution[i][jl][ml][tl]

        worst_r = 0;
		if(i != r) {
            for(j = nCells-1; j >= 0; j--) //Si puo' cambiare e partire da j = r-1
                if(j != r && j != i)
                    for(m = 0; m < nCustomerTypes; m++) //Guardando ogni tipo persona
                        for(t = 0; t < nTimeSteps; t++) //Ogni istante di tempo

                        //Controllo in piu' da fare rispetto a prima, ovvero se con quelle persone riuscirei a soddisfare i task del precedente
                            if(solution[r][j][m][t] != 0 && solution[r][j][m][t]*problem.n[m] >= task_todo) {

                                //Controllo che task_todo sia un multiplo del tipo persona
                                if(task_todo % problem.n[m] == 0) {
                                    people_to_move = task_todo/problem.n[m];
                                    int obj_gain = solution[r][j][m][t]*problem.costs[r][j][m][t];
                                    if(obj_gain > worst_r) {
                                        worst_r = obj_gain;
                                        jr = j;
                                        mr = m;
                                        tr = t;
                                    }
                                }
                            }
		}
        //Peggior apporto alla objFunc e' solution[i][jr][mr][tr]

        //Quanto mi costa scambiare?
        if(people_to_move != 0) {
            int gain_right = solution[r][jr][mr][tr]*problem.costs[r][jr][mr][tr] - people_to_move*problem.costs[r][jl][ml][tl];
            int gain_left = solution[i][jl][ml][tl]*problem.costs[i][jl][ml][tl] - solution[i][jl][ml][tl]*problem.costs[i][jr][mr][tr];

            if(gain_right + gain_left > 0) { //SWAP
                objFunc -= gain_left;
                objFunc -= gain_right;

                solution[i][jr][mr][tr] = solution[i][jl][ml][tl];
                solution[i][jl][ml][tl] -= solution[i][jl][ml][tl];
                solution[r][jl][ml][tl] = solution[r][jr][mr][tr];
                solution[r][jr][mr][tr] -= people_to_move;
            }

        }

        q++;
        i++;
        r--;
	}
}

typedef struct {
    int src;
    int dest;
    int type;
    int time;
    int cost;
} Min;


void Heuristic::solveFast(vector<double>& stat, int timeLimit) {
    double objFun=0;
    clock_t tStart = clock();

    for (int i = 0; i < nCells; i++)
        for (int j = 0; j < nCells; j++)
            for (int m = 0; m < nCustomerTypes; m++)
                for (int t = 0; t < nTimeSteps; t++)
                    solution[i][j][m][t] = 0;

    /*
    *   Computo una cella per volta cercando:
    *   quanti utenti provenienti da una qualsiasi altra cella (ricordare che un utente non puo' effettuare task nella cella origine)
    *   per ogni tipo di utente
    *   per ogni istante temporale
    *   la ricerca e' greedy perche' non appena il numero di task raggiunge quello richiesto si passa alla cella successiva
    */
    for (int i = 0; i < nCells; i++) {
        int demand = problem.activities[i];

        bool notSatisfied = true;
        for (int j = 0; j < nCells && notSatisfied; j++) {
            for (int m = 0; m < nCustomerTypes && notSatisfied; m++) {
                for (int t = 0; t < nTimeSteps && notSatisfied; t++) {
                    if (i != j) {
                        /*
                        * Se non é possibile soddisfare la richiesta usando tutte le persone derivanti dalla cella attuale,
                        * consideranto il tipo persona attuale (ovvero quanti task per cella puo' compiere)
                        * al tempo attuale, allora si prelevano tutte e si passa all'istante di tempo seguente
                        */
                        if (demand > problem.n[m] * problem.usersCell[j][m][t]) {
                            /*
                            * L'assegnazione alla soluzione in questo modo significa:
                            * e' assegnata la richiesta a questo numero di persone di tipo 'm'
                            * che si trovano ora nella cella j e devono andare nella cella i
                            */
                            solution[j][i][m][t] = problem.usersCell[j][m][t];
                            problem.usersCell[j][m][t] -= solution[j][i][m][t];
                        }
                        else {
                            /*
                            * Altrimenti si assegna un numero sufficiente di utenti di tipo 'm'
                            * per soddisfare tutti i task (intero). Si considera la cella come
                            * coperta e si passa a i+1
                            */
                            solution[j][i][m][t] += ceil((float) demand / problem.n[m]);
                            problem.usersCell[j][m][t] -= solution[j][i][m][t];
                            notSatisfied = false;
                        }
                        if (solution[j][i][m][t] != 0)
                            objFun += solution[j][i][m][t] * problem.costs[j][i][m][t];
                        demand -= problem.n[m]*solution[j][i][m][t];
                    }
                }
            }
        }
    }

    // Inserisce nel vettore delle statistiche il valore della objFunc finale e il tempo impiegato
    stat.push_back(objFun);
    double lastedTime = (double) (clock() - tStart) / CLOCKS_PER_SEC;
    stat.push_back(lastedTime);
    cout << "Tempo impiegato: " << ((double)(clock()-tStart) / CLOCKS_PER_SEC) << " s" << endl;

    hasSolution=true;
}

// Scrittura del file di output
void Heuristic::writeKPI(string path, string instanceName, vector<double> stat){
    if (!hasSolution)
        return;

    ofstream fileO(path, ios::app);
    if(!fileO.is_open())
        return;

    /* Key Performance Indicators
    * Scrivo: IstanceName;ValoreObjFunction;TempoImpiegato[sec]
    */
    fileO << instanceName << ";" << stat[0] << ";" << stat[1];
    // Aggiungo il conteggio di tutti i tipi utente
    for(int i=2; i<stat.size(); i++)
        fileO <<  ";" << stat[i];
    fileO << endl;

    fileO.close();
}
// Scrittura del file soluzione
void Heuristic::writeSolution(string path) {
    if (!hasSolution)
        return;

    ofstream fileO(path);
    if(!fileO.is_open())
        return;

    fileO << this->nCells << "; " << this->nTimeSteps << "; " << this->nCustomerTypes << endl;
    for (int m = 0; m < this->nCustomerTypes; m++)
        for (int t = 0; t < this->nTimeSteps; t++)
            for (int i = 0; i < this->nCells; i++)
                for (int j = 0; j < this->nCells; j++)
                    if (solution[i][j][m][t] > 0)
                        fileO << i << ";" << j << ";" << m << ";" << t << ";" << solution[i][j][m][t] << endl;

    fileO.close();
}

eFeasibleState Heuristic::isFeasible(string path) {

    string line;
    string word;
    int nCellsN;
    int nTimeStepsN;
    int nCustomerTypesN;
    int i, j, m, t;


    ifstream iffN(path.c_str());

    if (!iffN.is_open()) {
        cout << "Impossible to open" << path << endl;
        exit(1);
    }

    getline(iffN, line);
    std::replace(line.begin(), line.end(), ';', ' ');
    istringstream iss(line);
    iss >> word; // nCells
    nCellsN = atoi(word.c_str());
    iss >> word; // nTimeSteps
    nTimeStepsN = atoi(word.c_str());
    iss >> word; // nCustomerTypes
    nCustomerTypesN = atoi(word.c_str());

    int**** solutionN = new int***[nCells];
    for (i = 0; i < nCellsN; i++) {
        solutionN[i] = new int**[nCells];
        for (j = 0; j < nCellsN; j++) {
            solutionN[i][j] = new int*[nCustomerTypes];
            for (m = 0; m < nCustomerTypesN; m++) {
                solutionN[i][j][m] = new int[nTimeSteps];
                for ( t = 0; t < nTimeStepsN; t++) {
                    solutionN[i][j][m][t] = 0;
                }
            }
        }
    }

    while (getline(iffN, line)) {
        std::replace(line.begin(), line.end(), ';', ' ');
        istringstream iss(line);
        iss >> word; // i
        i = atoi(word.c_str());
        iss >> word; // j
        j = atoi(word.c_str());
        iss >> word; // m
        m = atoi(word.c_str());
        iss >> word; // t
        t = atoi(word.c_str());
        iss >> word; // value
        solutionN[i][j][m][t] = atoi(word.c_str());
    }

    // Demand
    bool feasible = true;
    int expr = 0;
    for (int i = 0; i < nCells && feasible; i++) {
        expr = 0;
        for (int j = 0; j < nCells; j++)
        	if (j != i)
        		for (int m = 0; m < nCustomerTypes; m++)
        			for (int t = 0; t < nTimeSteps; t++)
        				expr += problem.n[m] * solutionN[j][i][m][t];
        if (expr < problem.activities[i])
            feasible = false;
    }

    if (!feasible)
        return NOT_FEASIBLE_DEMAND;

    // Max Number of users
    for (int i = 0; i < nCells && feasible; i++)
        for (int m = 0; m < nCustomerTypes && feasible; m++)
            for (int t = 0; t < nTimeSteps && feasible; t++) {
                expr = 0;
                for (int j = 0; j < nCells; j++)
                    expr += solutionN[i][j][m][t];
                if (expr > problem.usersCell[i][m][t])
                    feasible = false;
            }

    if(!feasible)
        return NOT_FEASIBLE_USERS;

    return FEASIBLE;
}

/*
 * Si somma il numero di utenti coinvolti nella soluzione per ogni tipo
 * e lo si aggiunge in coda al vettore delle statistiche
 */
void Heuristic::getStatSolution(vector<double>& stat) {
    if (!hasSolution)
        return;

    int* tipi = new int[nCustomerTypes];
    for (int m = 0; m < nCustomerTypes; m++)
        tipi[m] = 0;

    for (int i = 0; i < nCells; i++)
        for (int j = 0; j < nCells; j++)
            for (int t = 0; t < nTimeSteps; t++)
                for (int m = 0; m < nCustomerTypes; m++)
                    if (solution[i][j][m][t] > 0)
                        tipi[m] += solution[i][j][m][t];
    for (int m = 0; m < nCustomerTypes; m++)
        stat.push_back(tipi[m]);

}

void Heuristic::checkOptimalityGap(string _optPath, string& instanceName, vector<double> stat) {
    string line, tmp, instName;
    double objVal = stat[0];
    double optVal;

    ifstream iffN(_optPath.c_str());
    if (!iffN.is_open()) {
        cout << "Impossible to open " << _optPath << endl;
        cin.get();
        exit(1);
    }

    do {
        getline(iffN, line);
        std::replace(line.begin(), line.end(), ';', ' ');
        istringstream iss(line);
        iss >> instName;
        iss >> tmp;
        iss >> tmp;
        optVal = atof(tmp.c_str());
    } while ( instanceName.compare(instName + ".txt") != 0);

    cout << "Optimality gap = " << ((objVal - optVal)/optVal)*100 << " %" << endl;
}